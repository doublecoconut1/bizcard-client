export const environment = {
  production: true,
  apiURL: 'https://prod-api.fizzcard.com/prod/'
};
