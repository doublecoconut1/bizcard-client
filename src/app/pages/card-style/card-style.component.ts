import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CardType } from '../../common/enums/card-types.enum';
import { fromEvent, timer } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserService } from '../../common/services/user.service';
import { User } from '../../common/interfaces/IUser.interface';
import { DomSanitizer } from '@angular/platform-browser';
import { isMobile } from '../../common/helpers';

declare var Hammer: any;

export enum SwipeType {
  Left = 'swipeleft',
  Right = 'swiperight',
}

@Component({
  selector: 'app-card-style',
  templateUrl: './card-style.component.html',
  styleUrls: ['./card-style.component.scss'],
})
export class CardStyleComponent implements OnInit, AfterViewInit {
  public image;
  public firstName: string;
  public lastName: string;
  public user: Partial<User>;
  public showSocials: boolean;
  public selectedCardIndex = 1;
  public cards = [CardType.Creative, CardType.Minimalist, CardType.Modern];
  public modern = CardType.Modern;
  public creative = CardType.Creative;
  public minimalist = CardType.Minimalist;

  @ViewChild('cardsStyleContainerRef', { static: false })
  cardsStyleContainerRef: ElementRef;
  @ViewChild('cardContainer') cardContainer: ElementRef;
  public demoCards: string[] = [
    CardType.Creative,
    CardType.Minimalist,
    CardType.Modern,
  ];
  public isMobile: boolean;
  public iframeScale = 1;
  private cardComponents = [];

  constructor(
    private router: Router,
    public sanitizer: DomSanitizer,
    private userService: UserService,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.isMobile = isMobile();
  }

  ngAfterViewInit(): void {
    this.calculateScale();
    if (this.isMobile) {
      this.cardComponents = this.cardsStyleContainerRef.nativeElement.querySelectorAll(
        '.parent-card-container'
      );
      this.addCardStyleSwipe();
    }
    this.user = this.userService.user;
    this.selectWithIndex(this.selectedCardIndex);
  }

  @HostListener('window:resize') handleResizeWindow(): void {
    this.calculateScale();
  }

  public selectWithIndex(index: number): void {
    this.userService.cardType = this.cards[index];
    this.selectedCardIndex = index;
  }

  public chooseCardStyle(cardType: CardType): void {
    this.userService.cardType = cardType;
    this.router.navigateByUrl('/preview').then((r) => r);
  }

  private calculateScale(): void {
    this.cd.detectChanges();
    if (this.isMobile) {
      const { height, width } = document
        .querySelector('.cards')
        .getBoundingClientRect();
      this.iframeScale = Math.min(height / 900, width / 914);
    } else {
      const iframeContainer = this.cardContainer.nativeElement.querySelector(
        '.iframe-container'
      );
      const { height } = iframeContainer.getBoundingClientRect();
      this.iframeScale = height / 900;
    }
    this.cd.detectChanges();
  }

  private addCardStyleSwipe(): void {
    timer(50)
      .pipe()
      .subscribe(() => {
        this.cardComponents = this.cardsStyleContainerRef.nativeElement.querySelectorAll(
          '.parent-card-container'
        );
        const mc = new Hammer(this.cardsStyleContainerRef.nativeElement);
        mc.on('swipeleft swiperight', (e: any) => {
          e.srcEvent.preventDefault();
          e.srcEvent.stopPropagation();
          if (e.type === SwipeType.Left) {
            this.selectWithIndex(
              Math.min(this.selectedCardIndex + 1, this.cards.length - 1)
            );
          } else if (e.type === SwipeType.Right) {
            this.selectWithIndex(Math.max(this.selectedCardIndex - 1, 0));
          } else if (e.type === 'tap') {
          }
        });
        this.listToWindowResize();
      });
  }

  private listToWindowResize(): void {
    fromEvent(window, 'resize')
      .pipe(distinctUntilChanged(), debounceTime(300), startWith(0))
      .subscribe(() => {
        this.setTemplateListDeltaWidth();
      });
  }

  private setTemplateListDeltaWidth(): void {}
}
