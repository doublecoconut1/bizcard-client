import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

import { User, UserForBack } from '../../common/interfaces/IUser.interface';
import { RestService } from '../../common/services/rest.service';
import { CardType } from '../../common/enums/card-types.enum';
import { UserService } from '../../common/services/user.service';
import { isMobile } from '../../common/helpers';
import { WallpaperGeneratorService } from '../../common/services/wallpaper-generator.service';

@Component({
  selector: 'app-card-wrapper',
  templateUrl: './card-wrapper.component.html',
  styleUrls: ['./card-wrapper.component.scss'],
})
export class CardWrapperComponent implements OnInit {
  public isLoad = false;
  @Input() image;
  @Input() user: Partial<User>;
  @Input() cardType: CardType = CardType.Modern;
  @Input() showSocials: boolean;
  @Input() isCenter = true;

  public firstName: string;
  public lastName: string;
  public modern = CardType.Modern;
  public creative = CardType.Creative;
  public minimalist = CardType.Minimalist;
  public disabledEvent: boolean;
  public parentFrameUserName: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private userService: UserService,
    private wallpaperGeneratorService: WallpaperGeneratorService,
    private restService: RestService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    const name = this.activatedRoute.snapshot.params.name;
    if (name === 'demo') {
      this.userService.demoLoad$.subscribe(
        ({ user, card, image, disabledEvent, userName }) => {
          this.firstName = user.personalInfo.firstName;
          this.lastName = user.personalInfo.lastName;
          this.parentFrameUserName = userName;
          this.changeModelUserToUI({ ...user, image });
          this.cardType = card;
          this.isLoad = true;
          this.disabledEvent = disabledEvent;
          this.changeDetectorRef.detectChanges();
        }
      );
    } else if (name) {
      if (isMobile()) {
        this.getUserInfo(name);
      } else {
        this.router.navigate(['name', name]);
      }
    }
  }

  public handleSaveToHome(userName: string): void {
    const isIframe = window.parent !== window;
    const dimensions = isIframe
      ? {
          width: window.document.documentElement.clientWidth,
          height: window.document.documentElement.clientHeight,
        }
      : window.screen;
    this.wallpaperGeneratorService.generateHomeScreenImage(
      {
        width: dimensions.width,
        height: dimensions.height,
      },
      `${window.origin}/#/${isIframe ? this.parentFrameUserName : userName}`
    );
  }

  public handleShare(userName: string): void {
    const isIframe = window.parent !== window;
    if (isIframe) {
      window.parent.postMessage(
        {
          type: 'action',
          action: 'share',
          url: `${window.origin}/#/${this.parentFrameUserName}`,
        },
        '*'
      );
    } else {
      this.userService.share(`${window.origin}/#/${userName}`);
    }
  }

  private changeModelUserToUI(data: Partial<UserForBack>): void {
    const { image, type, personalInfo, socialLinks, contact, location } = data;
    this.cardType = type as CardType;
    this.user = {
      image,
      personalInfo: { ...personalInfo },
      socialLinks,
      contact,
      location,
    };
    this.image = this.sanitizer.bypassSecurityTrustStyle('url(' + image + ')');
    this.showSocials = Object.values(this.user.socialLinks).some(
      (v) => v && v.length > 0
    );
  }

  private getUserInfo(name: string): void {
    this.restService.getUser(name).subscribe(
      (user: Partial<UserForBack>) => {
        const [firstName, lastName] = this.userService.initUserName(user.name);
        this.firstName = firstName;
        this.lastName = lastName;

        const fullData = {
          image: user.image,
          contact: user.contact,
          socialLinks: user.socialLinks,
          personalInfo: {
            firstName,
            lastName,
            title: user.title,
            company: user.company,
          },
          location: user.location,
          type: user.type,
        };
        this.changeModelUserToUI(fullData);
        this.isLoad = true;
      },
      () => {
        this.router.navigateByUrl('/');
      }
    );
  }
}
