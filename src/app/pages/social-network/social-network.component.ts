import {Component, ElementRef, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NetworkTypes} from '../../common/enums/network-types.enum';
import {Router} from '@angular/router';
import {UserService} from '../../common/services/user.service';
import {FunctionalityHelpersService} from '../../common/helpers/functionality-helpers.service';

@Component({
  selector: 'app-social-network',
  templateUrl: './social-network.component.html',
  styleUrls: ['./social-network.component.scss']
})
export class SocialNetworkComponent implements OnInit {

  public networksInfoForm: FormGroup;
  public socialNetworks = Object.values(NetworkTypes);
  private urlRegEx = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;

  @ViewChildren('socialElements') socialElements: QueryList<ElementRef>;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private functionalityHelpersService: FunctionalityHelpersService
  ) {
  }

  ngOnInit(): void {
    this.initForms();
    this.initSocialData();
  }

  public submitData(): void {
    const links = this.networksInfoForm.getRawValue();
    Object.keys(links).forEach(k =>
      links[k] = this.functionalityHelpersService.formatURL(links[k])
    );
    this.userService.user.socialLinks = links;
    this.router.navigateByUrl('card-create/card-style');
  }

  private initForms(): void {
    this.networksInfoForm = this.formBuilder.group({
      facebook: ['', Validators.pattern(this.urlRegEx)],
      twitter: ['', Validators.pattern(this.urlRegEx)],
      instagram: ['', Validators.pattern(this.urlRegEx)],
      linkedin: ['', Validators.pattern(this.urlRegEx)],
      youtube: ['', Validators.pattern(this.urlRegEx)],
      snapchat: ['', Validators.pattern(this.urlRegEx)],
      tiktok: ['', Validators.pattern(this.urlRegEx)],
      soundcloud: ['', Validators.pattern(this.urlRegEx)],
    });
  }

  private initSocialData(): void {
    if (this.userService.user.socialLinks) {
      this.networksInfoForm.patchValue(this.userService.user.socialLinks);
    }
  }

  public scroll(): void {
    const lastElement = this.socialElements.last.nativeElement;
    lastElement.scrollIntoView();
  }
}
