import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UserService } from '../../common/services/user.service';
import { CardType } from '../../common/enums/card-types.enum';
import { RestService } from '../../common/services/rest.service';

@Component({
  selector: 'app-card-demo',
  templateUrl: './card-demo.component.html',
  styleUrls: ['./card-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardDemoComponent implements OnInit {
  @ViewChild('iframe', { static: true, read: ElementRef })
  iframe: ElementRef<HTMLIFrameElement>;
  @Input() selected: boolean;
  @Input() card: CardType;
  @Input() iframeScale: number;
  @Input() iframeClass: string;
  @Input() transformOrigin = 'top';
  @Input() disabledEvent = false;
  @Input() userName: string;
  @Input() width = 477;
  @Input() height = 859;

  public url: SafeResourceUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(
    `${window.origin}/#/demo`
  );

  constructor(
    private domSanitizer: DomSanitizer,
    private userService: UserService,
    private restService: RestService
  ) {}

  ngOnInit(): void {}

  public handleLoadEnd(event): void {
    let user = this.userService.user;
    let cardType = this.card;
    let userImage = this.userService.userImageFile
      ? URL.createObjectURL(this.userService.userImageFile)
      : null;
    if (this.restService.user) {
      const {
        image,
        type,
        name,
        title,
        company,
        socialLinks,
        contact,
        location,
      } = this.restService.user;
      const [firstName, lastName] = this.userService.initUserName(name);
      cardType = type as CardType;
      user = {
        image,
        personalInfo: {
          firstName,
          lastName,
          title,
          company,
        },
        socialLinks,
        contact,
        location,
      };
      userImage = user.image;
    }
    this.iframe.nativeElement.contentWindow.postMessage(
      {
        type: 'demo',
        disabledEvent: this.disabledEvent,
        image: userImage,
        userName: this.userName,
        card: cardType,
        user,
      },
      '*'
    );
  }
}
