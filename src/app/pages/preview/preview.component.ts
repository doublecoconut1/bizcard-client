import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from '../../common/services/user.service';
import { User, UserForBack } from '../../common/interfaces/IUser.interface';
import { CardType } from '../../common/enums/card-types.enum';
import { RestService } from '../../common/services/rest.service';
import { switchMap, tap } from 'rxjs/operators';
import { S3UploadData } from '../../common/interfaces/S3UploadData';
import { FunctionalityHelpersService, isMobile } from '../../common/helpers';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss'],
})
export class PreviewComponent implements OnInit, AfterViewInit {
  @ViewChild('cardParent', { static: true, read: ElementRef }) cardParent;

  public isLoad = false;
  public image;
  public firstName: string;
  public lastName: string;
  public user: Partial<User>;
  public cardType: CardType;
  public isOptions = true;
  public showSocials: boolean;
  public modern = CardType.Modern;
  public creative = CardType.Creative;
  public minimalist = CardType.Minimalist;
  public iframeScale: number;

  constructor(
    private router: Router,
    private sanitizer: DomSanitizer,
    private restService: RestService,
    private userService: UserService,
    private functionalityHelpersService: FunctionalityHelpersService
  ) {}

  ngOnInit(): void {
    this.isOptions = true;
    this.user = this.userService.user;
    this.cardType = this.userService.cardType;
    this.calculateScale();
  }

  @HostListener('window:resize') handleResizeWindow(): void {
    this.calculateScale();
  }

  ngAfterViewInit(): void {
    this.calculateScale();
  }

  public handleClick(): void {
    this.isLoad = true;
    let url: string;
    const name = (
      this.user.personalInfo.firstName + this.user.personalInfo.lastName
    )
      .toUpperCase()
      .replace(/\s+|_+/g, '_')
      .concat('.png');

    const modifiedData: Partial<UserForBack> = this.changeModelUserData(
      this.user
    );
    this.restService
      .getUploadURL(name)
      .pipe(
        tap((s3: S3UploadData) => ([url] = s3.url.split('?'))),
        switchMap((s3: S3UploadData) =>
          this.restService.uploadFile(s3.url, this.userService.userImageFile)
        ),
        tap(() => (modifiedData.image = url)),
        switchMap(() => this.restService.saveUser(modifiedData))
      )
      .subscribe((user: Partial<UserForBack>) => {
        this.isLoad = false;
        const redirectTo = isMobile()
          ? this.router.navigate([`${'/' + user.username}`])
          : this.router.navigate([`name`, user.username]);
        redirectTo.then((data) => {
          this.userService.reset();
        });
      });
  }

  private initUserName(name: string): void {
    const nameParts = name.split(' ');
    this.firstName = nameParts[0];
    nameParts.shift();
    this.lastName = nameParts.join(' ').toString();
  }

  private changeModelUserData(data: Partial<User>): Partial<UserForBack> {
    data.contact.website = this.functionalityHelpersService.formatURL(
      data.contact.website
    );
    const personalInfo = {
      company: data.personalInfo.company,
      name: data.personalInfo.firstName + ' ' + data.personalInfo.lastName,
      title: data.personalInfo.title,
      type: this.cardType,
    };
    return {
      ...personalInfo,
      socialLinks: data.socialLinks,
      contact: data.contact,
      location: data.location,
    };
  }

  private calculateScale(): void {
    const { height } = this.cardParent.nativeElement.getBoundingClientRect();
    this.iframeScale = (height - 226) / height;
  }
}
