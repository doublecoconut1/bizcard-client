import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isMobile } from '../../common/helpers/animations';
import { UserService } from '../../common/services/user.service';
import { RestService } from '../../common/services/rest.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-final-page',
  templateUrl: './final-page.component.html',
  styleUrls: ['./final-page.component.scss'],
})
export class FinalPageComponent implements OnInit, AfterViewInit {
  public url = 'http://fizzcard.com/sample2131341';
  public isMobile: boolean;
  public iframeScale: number;
  @ViewChild('cardParent', { static: true, read: ElementRef }) cardParent;
  private userName: string;
  public copied!: boolean;

  constructor(
    private router: Router,
    public userService: UserService,
    public restService: RestService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.userName = this.activatedRoute.snapshot.paramMap.get('name');
    this.url = `${window.origin}/#/${this.userName}`;
    this.restService.getUser(this.userName).subscribe();
    this.isMobile = isMobile();
    if (this.isMobile) {
      this.router.navigate([`${'/' + this.userName}`]);
    }
  }

  public handleLinkCopy(link: string): void {
    this.copyLink(link);
  }

  public onShare(): void {
    if (this.isMobile) {
      this.userService.share(this.url);
    }
  }

  ngAfterViewInit(): void {
    this.onResize();
  }

  @HostListener('window:resize')
  public onResize(): void {
    // const { height } = this.cardParent.nativeElement.getBoundingClientRect();
    // this.iframeScale = height / 635;
  }

  private copyLink(link: string): void {
    const inputElement = document.createElement('input');
    document.body.appendChild(inputElement);
    inputElement.setAttribute('value', `${link}`);
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    inputElement.parentNode.removeChild(inputElement);
    this.copied = true;
  }
}
