import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../common/services/user.service';
import { isMobile } from '../../common/helpers';

@Component({
  selector: 'app-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.scss'],
})
export class ContactInfoComponent implements OnInit {
  public isErrorVisible = false;
  public contactInfoForm: FormGroup;
  private urlRegEx = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
  private emailRegEx = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
  public isMobile: boolean;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.isMobile = isMobile();
    this.initForm();
    this.initUserContacts();
  }

  public handleClick(): void {
    if (this.contactInfoForm.invalid) {
      this.isErrorVisible = true;
    } else {
      this.isErrorVisible = false;
      this.userService.user.contact = this.contactInfoForm.getRawValue();
      this.router.navigateByUrl('/card-create/social-info');
    }
  }

  private initForm(): void {
    this.contactInfoForm = this.formBuilder.group({
      email: ['', [Validators.pattern(this.emailRegEx)]],
      website: [
        '',
        [Validators.pattern(this.urlRegEx), Validators.maxLength(80)],
      ],
      phone: ['', [Validators.minLength(3)]],
    });
  }

  private initUserContacts(): void {
    if (this.userService.user.contact) {
      this.contactInfoForm.patchValue(this.userService.user.contact);
    }
  }
}
