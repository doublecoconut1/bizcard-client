import {ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {UserService} from '../../common/services/user.service';
import {Router} from '@angular/router';
import {Overflow} from '../../common/enums/overflow-types.enum';

declare var Hammer: any;

@Component({
  selector: 'app-choose-image',
  templateUrl: './choose-image.component.html',
  styleUrls: ['./choose-image.component.scss']
})
export class ChooseImageComponent implements OnInit, OnDestroy {
  public isNoteVisible = false;
  public isErrorVisible = false;
  public notificationText: string;
  public selectedFileStyle: SafeUrl;

  @ViewChild('canvasElement', {static: false}) public canvasElement: ElementRef<HTMLCanvasElement>;
  @ViewChild('input', {static: false}) public inputElement: ElementRef<HTMLCanvasElement>;

  public scale = 1;
  public offset = {
    x: 0,
    y: 0
  };

  private image: HTMLImageElement;

  constructor(
    private router: Router,
    private cd: ChangeDetectorRef,
    private sanitizer: DomSanitizer,
    private userService: UserService,
  ) {
  }

  private _selectedFile: File;

  public set selectedFile(value) {
    this._selectedFile = value;
    // this.selected$.emit(value);
  }

  private static setOverflowValue(overflowValue: Overflow): void {
    window.document.body.style.overflow = overflowValue;
    window.document.documentElement.style.overflow = overflowValue;
  }

  ngOnInit(): void {
    if (this.userService.userImageFile) {
      this.selectedFileStyle = this.initSafeUrl(this.userService.userImageFile);
      this._selectedFile = this.userService.userImageFile;
      this.loadImage();
      this.initHammer();
    }
  }

  ngOnDestroy(): void {
    ChooseImageComponent.setOverflowValue(Overflow.visible);
  }

  public handleImageSelect($event): void {
    const [file] = $event.target.files;
    if (!file) {
      return;
    }
    if (file.size > 4097152) {            // file size bigger than 4 MB
      this.isNoteVisible = true;
      this.notificationText = 'Your image is too big. Choose another image!';
      $event.target.value = null;
      return;
    }
    this.selectedFile = file;
    this.selectedFileStyle = this.initSafeUrl(file);
    this.loadImage();
    this.initHammer();
  }

  public handleClick(): void {
    if (!this.selectedFileStyle) {
      this.isErrorVisible = true;
    } else {
      this.isErrorVisible = false;
      const image = this.canvasElement.nativeElement.toDataURL('image/jpeg', 1.0);
      fetch(image)
        .then(res => res.blob())
        .then(blob => {
          this.userService.userImageFile = new File([blob], this._selectedFile.name, {type: 'image/jpeg'});
          this.router.navigateByUrl('/card-create/personal-info');
        });
    }
  }

  public handleScaleUpdate(step: number): void {
    this.scale = Math.min(Math.max(this.scale + step, 1), 3);
    this.drawImage();
  }

  private loadImage(): void {
    this.scale = 1;
    this.offset = {x: 0, y: 0};
    const img = new Image();
    img.src = URL.createObjectURL(this._selectedFile);
    img.onload = () => {
      this.image = img;
      this.drawImage();
    };
  }

  private drawImage(): void {
    this.canvasElement.nativeElement.height = this.image.height;
    this.canvasElement.nativeElement.width = this.image.width;
    const ctx = this.canvasElement.nativeElement.getContext('2d');
    ctx.clearRect(0, 0, this.canvasElement.nativeElement.width, this.canvasElement.nativeElement.height);
    const wrh = this.image.width / this.image.height;
    let newWidth = this.canvasElement.nativeElement.width;
    let newHeight = newWidth / wrh;
    if (newHeight > this.canvasElement.nativeElement.height) {
      newHeight = this.canvasElement.nativeElement.height;
      newWidth = newHeight * wrh;
    }
    const x = (this.offset.x * newWidth / this.canvasElement.nativeElement.clientWidth)
      + (this.canvasElement.nativeElement.width - newWidth) / 2;
    const y = (this.offset.y * newWidth / this.canvasElement.nativeElement.clientWidth)
      + (this.canvasElement.nativeElement.height - newHeight) / 2;
    newWidth *= this.scale;
    newHeight *= this.scale;
    ctx.drawImage(this.image, x, y, newWidth, newHeight);
  }

  private initSafeUrl(file: File): SafeUrl {
    return this.sanitizer.bypassSecurityTrustStyle('url(' + URL.createObjectURL(file) + ')');
  }

  private initHammer(): void {
    this.cd.detectChanges();
    const mc = new Hammer(this.canvasElement.nativeElement);
    mc.get('pinch').set({enable: true});
    let prev = {...this.offset};
    ChooseImageComponent.setOverflowValue(Overflow.hidden);
    mc.on('pan tap', (e: any) => {
      if (e.type === 'pan') {
        const x = prev.x + e.deltaX;
        const y = prev.y + e.deltaY;
        this.offset.x = Math.max(Math.min(x, 0),
          -(this.canvasElement.nativeElement.clientWidth * (this.scale - 1))
        );
        this.offset.y = Math.max(Math.min(y, 0),
          -(this.canvasElement.nativeElement.clientHeight * (this.scale - 1))
        );
        if (e.isFinal) {
          prev = {...this.offset};
        }
        this.drawImage();
      } else if (e.type === 'tap') {
        this.inputElement.nativeElement.click();
      }
    });
    let prevScale = 1;
    mc.on('pinch pinchend', (e) => {
      if (e.type === 'pinch') {
        this.scale = Math.min(Math.max(prevScale * e.scale, 1), 3);
      } else if (e.type === 'pinchend') {
        prevScale = this.scale;
      }
      this.drawImage();
    });
  }
}
