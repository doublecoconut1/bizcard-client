import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CardType } from '../../common/enums/card-types.enum';
import { RestService } from '../../common/services/rest.service';
import { UserService } from '../../common/services/user.service';
import { SwipeType } from '../card-style/card-style.component';
import { isMobile } from '../../common/helpers';

declare var Hammer: any;

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.scss'],
})
export class StartPageComponent implements OnInit, AfterViewInit {
  public cards = [CardType.Modern, CardType.Creative, CardType.Minimalist];
  public cardIndex = 1;
  public mainBg = 'creative_bg';
  public selectedType: CardType = CardType.Creative;
  @ViewChild('cardsContainer', { read: ElementRef, static: true })
  private cardsContainer: ElementRef<HTMLDivElement>;
  private elems = [];

  constructor(
    private restService: RestService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.restService.user = null;
    this.userService.reset();
    if (isMobile()) {
      this.initCardsSwipe();
    }
  }

  ngAfterViewInit(): void {
    this.initElements();
    this.checkOsAndSetValues();
  }

  public handleRightClick(): void {
    const nextCard = this.elems.filter(
      (card) => card.getAttribute('data-pos') === '1'
    );
    this.updateFunc(nextCard[0]);
    this.cardIndex = this.cardIndex >= 2 ? 0 : ++this.cardIndex;
    this.initMainBg();
  }

  public handleLeftClick(): void {
    const prevCard = this.elems.filter(
      (card) => card.getAttribute('data-pos') === '-1'
    );
    this.updateFunc(prevCard[0]);
    this.cardIndex = this.cardIndex <= 0 ? 2 : --this.cardIndex;
    this.initMainBg();
  }

  public handleCardClick(card: CardType): void {
    this.selectedType = card;
  }

  private checkOsAndSetValues(): void {
    if (navigator.appVersion.includes('Windows')) {
      const width = document.body.offsetWidth;
      const title = document.getElementsByClassName(
        'start-page_content_title'
      )[0] as HTMLDivElement;
      const text = document.getElementsByClassName(
        'start-page_content_text'
      )[0] as HTMLDivElement;
      if (width > 1680) {
        title.style.marginTop = '-77px';
        text.style.marginTop = '-31px';
      }
      if (width <= 1680 && width > 1440) {
        title.style.marginTop = '-74px';
        text.style.marginTop = '-14px';
      }

      if (width <= 1440 && width > 1024) {
        title.style.marginTop = '-74px';
        text.style.marginTop = '-12px';
      }

      if (width <= 1440 && width > 1024) {
      }
    }
  }

  private getPosFunction(current: any, active: any): number {
    const diff = current - active;
    if (Math.abs(current - active) > 1) {
      return -current;
    }
    return diff;
  }

  private updateFunc(newActive): void {
    const newActivePos = newActive.dataset.pos;
    const current = this.elems.find((elem) => elem.dataset.pos === '0');
    const prev = this.elems.find((elem) => elem.dataset.pos === '-1');
    const next = this.elems.find((elem) => elem.dataset.pos === '1');

    [current, prev, next].forEach((item) => {
      const itemPos = item.dataset.pos;

      item.dataset.pos = this.getPosFunction(itemPos, newActivePos);
    });
  }

  private initElements(): void {
    const carouselItems = document.querySelectorAll('.carousel_item');
    this.elems = Array.from(carouselItems);
  }

  private initMainBg(): void {
    switch (this.cardIndex) {
      case 0:
        this.mainBg = CardType.Minimalist + '_bg';
        break;
      case 1:
        this.mainBg = CardType.Creative + '_bg';
        break;
      case 2:
        this.mainBg = CardType.Modern + '_bg';
        break;
    }
  }

  private initMainMobileBg(): void {
    switch (this.cardIndex) {
      case 0:
        this.mainBg = CardType.Minimalist + '-mobile_bg';
        break;
      case 1:
        this.mainBg = CardType.Creative + '-mobile_bg';
        break;
      case 2:
        this.mainBg = CardType.Modern + '-mobile_bg';
        break;
    }
  }

  private initCardsSwipe(): void {
    const mc = new Hammer(this.cardsContainer.nativeElement);
    mc.on('swipeleft swiperight', (e: any) => {
      e.srcEvent.preventDefault();
      e.srcEvent.stopPropagation();
      const selectedIndex = this.cards.indexOf(this.selectedType);
      if (e.type === SwipeType.Left) {
        this.selectedType = this.cards[
          Math.min(Math.max(selectedIndex + 1, 0), this.cards.length - 1)
        ];
      } else if (e.type === SwipeType.Right) {
        this.selectedType = this.cards[
          Math.min(Math.max(selectedIndex - 1, 0), this.cards.length - 1)
        ];
      }
    });
  }
}
