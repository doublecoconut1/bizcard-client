import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../common/services/user.service';

@Component({
  selector: 'app-qr-code-page',
  templateUrl: './qr-code-page.component.html',
  styleUrls: ['./qr-code-page.component.scss']
})
export class QrCodePageComponent implements OnInit {
  private url: string;

  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    const userName = this.activatedRoute.snapshot.paramMap.get('name');
    this.url = `${window.origin}/#/${userName}`;
  }

  public onShare(): void {
    this.userService.share(this.url);
  }

  public handleLinkCopy(): void {
    const inputElement = document.createElement('input');
    document.body.appendChild(inputElement);
    inputElement.setAttribute('value',
      `${this.url}`);
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    inputElement.parentNode.removeChild(inputElement);
  }
}
