import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChooseImageComponent} from '../choose-image/choose-image.component';
import {ContactInfoComponent} from '../contact-info/contact-info.component';
import {CardStyleComponent} from '../card-style/card-style.component';
import {PersonalInfoComponent} from '../personal-info/personal-info.component';
import {SocialNetworkComponent} from '../social-network/social-network.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CardCreateRoutingModule} from './card-create-routing.module';
import {CardCreateComponent} from './pages/card-create/card-create.component';
import {PhoneMaskDirective} from '../../common/directives/phone-mask.directive';
import {CardDemoComponent} from '../card-demo/card-demo.component';
import {LetterSpacePipe} from '../../common/pipes/letter-space.pipe';
import {LocationComponent} from '../location/location.component';
import {CardContactInfoComponent} from '../../components/card-contact-info/card-contact-info.component';
import {CardLeftBarComponent} from '../../components/card-left-bar/card-left-bar.component';
import {LogoComponent} from '../../components/logo/logo.component';
import {CardViewComponent} from '../../components/card-view/card-view.component';
import {RemoveHttpPipe} from '../../common/pipes/remove-http.pipe';

@NgModule({
  declarations: [
    CardCreateComponent,
    ChooseImageComponent,
    ContactInfoComponent,
    CardStyleComponent,
    PersonalInfoComponent,
    LocationComponent,
    SocialNetworkComponent,
    PhoneMaskDirective,
    CardDemoComponent,
    LetterSpacePipe,
    CardContactInfoComponent,
    CardLeftBarComponent,
    LogoComponent,
    CardViewComponent,
    RemoveHttpPipe
  ],
  exports: [
    CardDemoComponent,
    LogoComponent,
    CardViewComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CardCreateRoutingModule,
  ]
})
export class CardCreateModule {
}
