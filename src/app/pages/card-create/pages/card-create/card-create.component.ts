import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {User} from '../../../../common/interfaces/IUser.interface';
import {RouterOutlet} from '@angular/router';
import {isMobile, routeAnimation} from '../../../../common/helpers/animations';

@Component({
  selector: 'app-card-create',
  templateUrl: './card-create.component.html',
  styleUrls: ['./card-create.component.scss'],
  animations: [routeAnimation]
})
export class CardCreateComponent implements OnInit {
  public user: User;
  isMobile: boolean;
  constructor(
    private changeDetectorRef: ChangeDetectorRef
  ) {
  }

  ngOnInit(): void {
    this.isMobile = isMobile();
    this.changeDetectorRef.detectChanges();
  }

  prepareRoute(outlet: RouterOutlet): RouterOutlet {
    return outlet.activatedRouteData.state ? outlet.activatedRouteData.state : '';
  }
}
