import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChooseImageComponent} from '../choose-image/choose-image.component';
import {ContactInfoComponent} from '../contact-info/contact-info.component';
import {CardStyleComponent} from '../card-style/card-style.component';
import {PersonalInfoComponent} from '../personal-info/personal-info.component';
import {CardCreateComponent} from './pages/card-create/card-create.component';
import {SocialNetworkComponent} from '../social-network/social-network.component';
import {AuthGuard} from '../../common/services/auth.guard';
import {LocationComponent} from '../location/location.component';

const cardCreateRoutes = [
  {
    path: '',
    component: ChooseImageComponent,
    data: {state: 'image-choose'}
  },
  {
    path: 'contact-info',
    component: ContactInfoComponent,
    data: {state: 'contact-info'}
  },
  {
    path: 'card-style',
    component: CardStyleComponent,
    // canActivate: [AuthGuard],
    data: {state: 'card-style'}
  },
  {
    path: 'personal-info',
    component: PersonalInfoComponent,
    data: {state: 'personal-info'}
  },
  {
    path: 'location',
    component: LocationComponent,
    data: {state: 'location'}
  },
  {
    path: 'social-info',
    component: SocialNetworkComponent,
    data: {state: 'social-info'}
  },
];

const routes: Routes = [
  {
    path: '',
    component: CardCreateComponent,
    children: cardCreateRoutes
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class CardCreateRoutingModule {
}
