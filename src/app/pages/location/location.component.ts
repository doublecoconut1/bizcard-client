import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../common/services/user.service';
import {LocationState} from '../../common/interfaces/IUser.interface';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  public isErrorVisible = false;
  public locationForm: FormGroup;
  public show = false;
  public locationStateData: LocationState[] = [];

  constructor(
    private router: Router,
    private userService: UserService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.initForms();
    this.initLocationData();
  }

  private initForms(): void {
    this.locationForm = this.formBuilder.group({
      address: ['', [Validators.minLength(2)]],
      address_second: ['', [Validators.minLength(2)]],
      city: ['', [Validators.minLength(2)]],
      state: ['', [Validators.minLength(2)]],
      zip: ['', [Validators.minLength(2)]],
    });
  }
  private initLocationData(): void {
    this.locationStateData = this.userService.getStateData();
    if (this.userService.user.location) {
      this.locationForm.patchValue(this.userService.user.location);
    }
  }
  public handleClick(): void {
    if (this.locationForm.invalid) {
      this.isErrorVisible = true;
    } else {
      this.isErrorVisible = false;
      this.userService.user.location = this.locationForm.getRawValue();
      this.router.navigateByUrl('card-create/contact-info');
    }
  }

}
