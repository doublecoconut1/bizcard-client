import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../common/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss'],
})
export class PersonalInfoComponent implements OnInit {
  public isErrorVisible = false;
  public personalInfoForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.initForms();
    this.initPersonalInfo();
  }

  public handleClick(): void {
    if (this.personalInfoForm.invalid) {
      this.isErrorVisible = true;
    } else {
      this.isErrorVisible = false;
      this.userService.user.personalInfo = this.personalInfoForm.getRawValue();
      this.router.navigateByUrl('card-create/location');
    }
  }

  private initForms(): void {
    this.personalInfoForm = this.formBuilder.group({
      firstName: [
        '',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(28),
        ],
      ],
      lastName: ['', [Validators.minLength(1), Validators.maxLength(32)]],
      title: ['', [Validators.minLength(2), Validators.maxLength(40)]],
      company: ['', [Validators.minLength(2), Validators.maxLength(40)]],
    });
  }

  private initPersonalInfo(): void {
    if (this.userService.user.personalInfo) {
      this.personalInfoForm.patchValue(this.userService.user.personalInfo);
    }
  }

  public checkingValue(event: Event, filed: string, limit): void {
    const val = (event.target as HTMLInputElement).value;
    if (val.length > limit) {
      this.personalInfoForm.get(filed).setValue(val.substring(0, limit));
    }
  }
}
