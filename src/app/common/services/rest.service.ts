import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable, of} from 'rxjs';
import {S3UploadData} from '../interfaces/S3UploadData';
import {User} from '../interfaces/IUser.interface';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  public user: any;

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  public getUploadURL(name: string): Observable<S3UploadData> {
    return this.httpClient.get<S3UploadData>(`${environment.apiURL}client/users/image-upload-url`, {params: {name}});
  }

  public uploadFile(url: string, file: File): Observable<any> {
    return this.httpClient.put(url, file);
  }

  public saveUser(user: Partial<User>): Observable<User> {
    return this.httpClient.post<User>(
      `${environment.apiURL}client/users/`, user
    ).pipe(tap((savedUser) => this.user = savedUser));
  }

  public getUser(name: string): Observable<User> {
    return this.user ? of(this.user) : this.httpClient.get<User>(`${environment.apiURL}client/users/${name}`)
      .pipe(tap((user) => this.user = user));
  }
}
