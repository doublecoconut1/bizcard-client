import { TestBed } from '@angular/core/testing';

import { WrapperGuard } from './wrapper.guard';

describe('WrapperGuard', () => {
  let guard: WrapperGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(WrapperGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
