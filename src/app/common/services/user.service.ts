import { Injectable } from '@angular/core';
import {
  Contact,
  LocationState,
  PersonalInfo,
  SocialLinks,
  User,
} from '../interfaces/IUser.interface';
import { CardType } from '../enums/card-types.enum';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public user: Partial<User> = {};
  public cardType: CardType;
  public userImageFile: File;

  private demoLoadSub = new ReplaySubject(1);
  public demoLoad$ = this.demoLoadSub.asObservable();

  constructor() {}

  public setUserInfo(user: User): void {
    this.user = user;
  }

  public setUserPersonalInfo(personalInfo: PersonalInfo): void {
    this.user.personalInfo = personalInfo;
  }

  public setUserImage(image: File): void {
    this.userImageFile = image;
  }

  public setSocialInfo(socialLinks: SocialLinks): void {
    this.user.socialLinks = socialLinks;
  }

  public setContactInfo(contactInfo: Contact): void {
    this.user.contact = contactInfo;
  }

  public initialize(): void {
    window.addEventListener('message', ({ data }) => {
      if (data.type === 'demo') {
        // TODO simplify with ...
        this.demoLoadSub.next({
          user: data.user,
          card: data.card,
          image: data.image,
          disabledEvent: data.disabledEvent,
          userName: data.userName,
        });
      }
    });
  }

  public share(url: string): void {
    const shareData = { url };
    if (navigator.share) {
      navigator
        .share(shareData)
        .then(() => console.log('Share was successful.'))
        .catch((error) => console.log('Sharing failed', error));
    } else {
      alert(`Your system doesn't support sharing files.`);
    }
  }

  public reset(): void {
    this.user = {};
    this.cardType = null;
    this.userImageFile = null;
  }

  public getStateData(): LocationState[] {
    return [
      {
        name: 'Alabama',
        code: 'AL',
      },
      {
        name: 'Alaska',
        code: 'AK',
      },
      {
        name: 'Arizona',
        code: 'AZ',
      },
      {
        name: 'Arkansas',
        code: 'AR',
      },
      {
        name: 'California',
        code: 'CA',
      },
      {
        name: 'Colorado',
        code: 'CO',
      },
      {
        name: 'Connecticut',
        code: 'CT',
      },
      {
        name: 'Delaware',
        code: 'DE',
      },
      {
        name: 'Florida',
        code: 'FL',
      },
      {
        name: 'Georgia',
        code: 'GA',
      },
      {
        name: 'Hawaii',
        code: 'HI',
      },
      {
        name: 'Idaho',
        code: 'ID',
      },
      {
        name: 'Illinois',
        code: 'IL',
      },
      {
        name: 'Indiana',
        code: 'IN',
      },
      {
        name: 'Iowa',
        code: 'IA',
      },
      {
        name: 'Kansas',
        code: 'KS',
      },
      {
        name: 'Kentucky',
        code: 'KY',
      },
      {
        name: 'Louisiana',
        code: 'LA',
      },
      {
        name: 'Maine',
        code: 'ME',
      },
      {
        name: 'Maryland',
        code: 'MD',
      },
      {
        name: 'Massachusetts',
        code: 'MA',
      },
      {
        name: 'Michigan',
        code: 'MI',
      },
      {
        name: 'Minnesota',
        code: 'MN',
      },
      {
        name: 'Mississippi',
        code: 'MS',
      },
      {
        name: 'Missouri',
        code: 'MO',
      },
      {
        name: 'Montana',
        code: 'MT',
      },
      {
        name: 'Nebraska',
        code: 'NE',
      },
      {
        name: 'Nevada',
        code: 'NV',
      },
      {
        name: 'New Hampshire',
        code: 'NH',
      },
      {
        name: 'New Jersey',
        code: 'NJ',
      },
      {
        name: 'New Mexico',
        code: 'NM',
      },
      {
        name: 'New York',
        code: 'NY',
      },
      {
        name: 'North Carolina',
        code: 'NC',
      },
      {
        name: 'North Dakota',
        code: 'ND',
      },
      {
        name: 'Ohio',
        code: 'OH',
      },
      {
        name: 'Oklahoma',
        code: 'OK',
      },
      {
        name: 'Oregon',
        code: 'OR',
      },
      {
        name: 'Pennsylvania',
        code: 'PA',
      },
      {
        name: 'Puerto Rico',
        code: 'PR',
      },
      {
        name: 'Rhode Island',
        code: 'RI',
      },
      {
        name: 'South Carolina',
        code: 'SC',
      },
      {
        name: 'South Dakota',
        code: 'SD',
      },
      {
        name: 'Tennessee',
        code: 'TN',
      },
      {
        name: 'Texas',
        code: 'TX',
      },
      {
        name: 'Utah',
        code: 'UT',
      },
      {
        name: 'Vermont',
        code: 'VT',
      },
      {
        name: 'Virginia',
        code: 'VA',
      },
      {
        name: 'Washington',
        code: 'WA',
      },
      {
        name: 'West Virginia',
        code: 'WV',
      },
      {
        name: 'Wisconsin',
        code: 'WI',
      },
      {
        name: 'Wyoming',
        code: 'WY',
      },
    ];
  }

  public initUserName(name: string): string[] {
    const nameParts = name.split(' ');
    const firstName = nameParts.shift();
    const lastName = nameParts.join(' ').toString();
    return [firstName, lastName];
  }

  public isEmptyObject(obj): boolean {
    return !Object.values(obj).some((element) => element !== null);
  }

  public orderObject(obj): any {
    return Object.keys(obj)
      .sort()
      .reduce((r, k) => ((r[k] = obj[k]), r), {});
  }
}
