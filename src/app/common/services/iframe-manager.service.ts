import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class IframeManagerService {
  constructor(private router: Router, private userService: UserService) {}

  public initialize(): void {
    window.addEventListener('message', ({ data }) => {
      switch (data.type) {
        case 'action': {
          switch (data.action) {
            case 'goToMain':
              this.router.navigateByUrl('/');
              break;
            case 'share':
              this.userService.share(data.url);
          }
          break;
        }
      }
    });
  }
}
