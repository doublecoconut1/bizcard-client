import {ApplicationRef, ComponentFactoryResolver, Injectable, Injector} from '@angular/core';
import {QRCodeComponent} from 'angularx-qrcode';

@Injectable({
  providedIn: 'root'
})
export class WallpaperGeneratorService {

  constructor(
    protected injector: Injector,
    protected appRef: ApplicationRef,
    protected componentFactoryResolver: ComponentFactoryResolver
  ) {
  }

  public async generateHomeScreenImage(windowDimensions: any, url: string): Promise<void> {
    const canvas = await this.generateQR(windowDimensions, url);
    const mainCanvas = document.createElement('canvas');
    mainCanvas.width = windowDimensions.width;
    mainCanvas.height = windowDimensions.height;
    const ctx = mainCanvas.getContext('2d');
    const [qrBlob, logoBlob] = await Promise.all([
      fetch(canvas.toDataURL('image/png', 1.0))
        .then(r => r.blob()),
      fetch('/assets/images/logos/logo_2_dark.png')
        .then(r => r.blob())
    ]);
    ctx.beginPath();
    ctx.rect(0, 0, mainCanvas.width, mainCanvas.height);
    ctx.fillStyle = 'rgba(253, 43, 77, 0.78)';

    const qrImage = new Image();
    qrImage.src = URL.createObjectURL(qrBlob);

    const logoImage = new Image();
    logoImage.src = URL.createObjectURL(logoBlob);

    logoImage.onload = () => {
      const qrX = (windowDimensions.width / 2) - qrImage.width / 2;
      const qrY = windowDimensions.height * 0.41;
      const logoScale = windowDimensions.width * 0.19 / logoImage.width;
      const scaledLogo = this.scaleIt(logoImage, logoScale);
      ctx.drawImage(
        scaledLogo, (windowDimensions.width / 2) - logoImage.width * logoScale / 2,
        windowDimensions.height * 0.31,
        logoImage.width * logoScale, logoImage.height * logoScale);
      this.roundedImage(ctx, qrX, qrY, qrImage.width, qrImage.height, 22);
      ctx.clip();
      ctx.drawImage(
        qrImage,
        qrX,
        qrY
      );
      this.download(mainCanvas);
    };
    ctx.fill();
  }

  private download(canvas: HTMLCanvasElement): void {
    const link = document.createElement('a');
    link.download = 'wallpaper.png';
    link.href = canvas.toDataURL('image/png');
    link.click();
  }

  private scaleIt(source, scaleFactor): HTMLCanvasElement {
    const c = document.createElement('canvas');
    const ctx = c.getContext('2d');
    const w = source.width * scaleFactor;
    const h = source.height * scaleFactor;
    c.width = w;
    c.height = h;
    ctx.drawImage(source, 0, 0, w, h);
    return (c);
  }

  private roundedImage(ctx, x, y, width, height, radius): void {
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();
  }

  private generateQR(windowDimensions: any, url: string): Promise<HTMLCanvasElement> {
    return new Promise((resolve) => {
      const component = this.componentFactoryResolver
        .resolveComponentFactory(QRCodeComponent)
        .create(this.injector);
      this.appRef.attachView(component.hostView);
      component.instance.qrdata = url;
      component.instance.width = windowDimensions.width * 0.7;
      component.instance.level = 'M';
      component.changeDetectorRef.detectChanges();
      component.instance.ngOnChanges();
      setTimeout(() => {
        const canvas = component.location.nativeElement.querySelector('canvas');
        resolve(canvas);
      }, 100);
    });
  }
}
