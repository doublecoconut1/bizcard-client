export interface User {
  image: string;
  personalInfo: PersonalInfo;
  socialLinks: SocialLinks;
  contact: Contact;
  location: Location;
}

export interface UserSocialLinks {
  [key: string]: SocialLinks;
}
export interface UserForBack {
  id?: string;
  image: string;
  name: string;
  title?: string;
  company?: string;
  socialLinks: SocialLinks;
  contact: Contact;
  username?: string;
  type: string;
  personalInfo?: PersonalInfo;
  location?: Location,
}

export class PersonalInfo {
  firstName: string;
  lastName: string;
  title?: string;
  company?: string;
}

export interface Contact {
  email: string;
  website?: string;
  phone?: string;
}
export interface Location {
  address?: string;
  address_second?: string;
  city?: string;
  state?: string;
  zip?: string;
}
export interface SocialLinks {
  facebook?: string;
  twitter?: string;
  instagram?: string;
  linkedin?: string;
  youtube?: string;
  snapchat?: string;
  tiktok?: string;
  soundcloud?: string;
}

export interface LocationState {
  name: string;
  code: string;
}

export interface IUserContactInfo {
  key: string;
  title: string;
  value: string;
  headerText: string;
  linkHref: string;
  btnText: string;
}
