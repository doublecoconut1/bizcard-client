import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'letterSpace'
})
export class LetterSpacePipe implements PipeTransform {

  transform(value: string): string {
    return value.split('').join(' ');
  }

}
