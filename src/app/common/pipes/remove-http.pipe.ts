import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeHttp'
})
export class RemoveHttpPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return value.replace('http://', '');
  }

}
