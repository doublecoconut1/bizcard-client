import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FunctionalityHelpersService {

  constructor() {
  }

  public formatURL(url: string): string {
    if (url.length) {
      return 'http://' + url.split('https://').join('').split('http://').join('');
    }
    return url;
  }
}
