export const getStyle = (el, styleProp) => {
  const camelize = (str) =>
    str.replace(/\-(\w)/g, (str2, letter) => letter.toUpperCase());

  if (el.currentStyle) {
    return el.currentStyle[camelize(styleProp)];
  } else if (document.defaultView && document.defaultView.getComputedStyle) {
    return document.defaultView
      .getComputedStyle(el, null)
      .getPropertyValue(styleProp);
  } else {
    return el.style[camelize(styleProp)];
  }
};
