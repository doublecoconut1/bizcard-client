import {
  animate,
  group,
  query as q,
  style,
  transition,
  trigger,
} from '@angular/animations';

export function isMobile(): boolean {
  if (window.screen.width === 1024 && window.screen.height === 1366) {
    return false;
  }
  return !!navigator.userAgent.match(
    /iPad|iPhone|Android|BlackBerry|Windows Phone|webOS/i
  );
}

const query = (s, a, o = { optional: true }) => q(s, a, o);

const height = isMobile() ? '!' : 'fit-content';

export const routeAnimation = trigger('routeAnimations', [
  transition(
    'start => card-create, card-create => image-choose,' +
      'image-choose => personal-info, personal-info => location, location => contact-info, contact-info => social-info,' +
      'social-info => card-style,card-create => preview-page',
    [
      style({ height }),
      query(':enter', style({ height, transform: 'translateX(100%)' })),
      query(
        ':enter, :leave',
        style({
          height,
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
        })
      ),
      group([
        query(':leave', [
          animate(
            '0.4s cubic-bezier(.35,0,.25,1)',
            style({ transform: 'translateX(-100%)' })
          ),
        ]),
        query(
          ':enter',
          animate(
            '0.4s cubic-bezier(.35,0,.25,1)',
            style({ transform: 'translateX(0)' })
          )
        ),
      ]),
    ]
  ),

  transition(
    'void => *, * => void,  start => card-create, start => image-choose, image-choose => card-create, card-create => start,' +
      ' image-choose => start, personal-info => image-choose, location => personal-info,' +
      ' contact-info => location, social-info => contact-info, card-style => social-info, preview-page => card-create, preview-page => card-style',
    [
      style({ height }),
      query(':enter', style({ height, transform: 'translateX(-100%)' })),
      query(
        ':enter, :leave',
        style({
          height,
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
        })
      ),
      group([
        query(':leave', [
          animate(
            '0.4s cubic-bezier(.35,0,.25,1)',
            style({ transform: 'translateX(100%)', height })
          ),
        ]),
        query(
          ':enter',
          animate(
            '0.4s cubic-bezier(.35,0,.25,1)',
            style({ transform: 'translateX(0)' })
          )
        ),
      ]),
    ]
  ),
]);

export const routerAnimationDescTop = trigger('routerAnimationDescTop', [
  transition('start => card-create', [
    query(':enter', style({ transform: 'translateX(100%)' })),
    query(
      ':enter, :leave',
      style({ position: 'absolute', top: 0, left: 0, right: 0 })
    ),
    group([
      query(':leave', [
        animate(
          '0.3s cubic-bezier(.35,0,.25,1)',
          style({ transform: 'translateX(-100%)' })
        ),
      ]),
      query(
        ':enter',
        animate(
          '0.4s cubic-bezier(.35,0,.25,1)',
          style({ transform: 'translateX(0)' })
        )
      ),
    ]),
  ]),
]);
