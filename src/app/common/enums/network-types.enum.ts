export enum NetworkTypes {
  Facebook = 'facebook',
  Linkedin = 'linkedin',
  Twitter = 'twitter',
  Instagram = 'instagram',
  Youtube = 'youtube',
  Snapchat = 'snapchat',
  Tiktok = 'tiktok',
  Soundcloud = 'soundcloud',
}
