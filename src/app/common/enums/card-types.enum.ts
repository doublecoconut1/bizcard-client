export enum CardType {
  Minimalist = 'minimalist',
  Creative = 'creative',
  Modern = 'modern'
}
