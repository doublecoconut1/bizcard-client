import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { StartPageComponent } from './pages/start-page/start-page.component';
import { PreviewComponent } from './pages/preview/preview.component';
import { FinalPageComponent } from './pages/final-page/final-page.component';
import { CardWrapperComponent } from './pages/card-wrapper/card-wrapper.component';
import { AuthGuard } from './common/services/auth.guard';
import { WrapperGuard } from './common/services/wrapper.guard';
import { QrCodePageComponent } from './pages/qr-code-page/qr-code-page.component';
import { EmailTemplateComponent } from './pages/email-template/email-template.component';

const routes: Routes = [
  {
    path: '',
    component: StartPageComponent,
    canActivate: [WrapperGuard],
    data: { state: 'start' },
  },
  {
    path: 'card-create',
    loadChildren: () =>
      import('./pages/card-create/card-create.module').then(
        (m) => m.CardCreateModule
      ),
    data: { state: 'card-create' },
  },
  {
    path: 'preview',
    component: PreviewComponent,
    canActivate: [AuthGuard],
    data: { state: 'preview-page' },
  },
  {
    path: 'name/:name',
    component: FinalPageComponent,
  },
  {
    path: 'template',
    component: EmailTemplateComponent,
  },
  {
    path: ':name',
    component: CardWrapperComponent,
  },
  {
    path: 'qr/:name',
    component: QrCodePageComponent,
  },

  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      scrollPositionRestoration: 'top',
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
