import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StartPageComponent} from './pages/start-page/start-page.component';
import {PreviewComponent} from './pages/preview/preview.component';
import {FinalPageComponent} from './pages/final-page/final-page.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CardCreateModule} from './pages/card-create/card-create.module';
import {QrComponent} from './components/qr/qr.component';
import {QRCodeModule} from 'angularx-qrcode';
import {HttpClientModule} from '@angular/common/http';
import {CardWrapperComponent} from './pages/card-wrapper/card-wrapper.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { QrCodePageComponent } from './pages/qr-code-page/qr-code-page.component';
import { EmailTemplateComponent } from './pages/email-template/email-template.component';


@NgModule({
  declarations: [
    AppComponent,
    StartPageComponent,
    PreviewComponent,
    FinalPageComponent,
    QrComponent,
    CardWrapperComponent,
    QrCodePageComponent,
    EmailTemplateComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    CardCreateModule,
    QRCodeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
