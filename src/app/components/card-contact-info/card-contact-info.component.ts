import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CardType} from '../../common/enums/card-types.enum';

@Component({
  selector: 'app-card-contact-info',
  templateUrl: './card-contact-info.component.html',
  styleUrls: ['./card-contact-info.component.scss']
})
export class CardContactInfoComponent {
  @Input() contactInfo;
  @Input() selected: any;
  @Input() cardType: CardType;
  @Output() selectEvent?: EventEmitter<unknown> = new EventEmitter();
  public cardTypeEnum = CardType;
  @ViewChild('contactEl', {read: ElementRef, static: true}) private contactEl: ElementRef;

  constructor() {
  }

  @HostListener('document:click', ['$event'])
  onClick(e: MouseEvent): void {
    const event = e as any;
    const clickedInside = (event.path || event.composedPath())
      .includes(this.contactEl.nativeElement);
    if (!clickedInside) {
      this.selected.index = null;
    }
  }

}
