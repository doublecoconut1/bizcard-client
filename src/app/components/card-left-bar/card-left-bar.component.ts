import {Component, Input, OnInit, Renderer2} from '@angular/core';
import {BaseCardComponent} from '../BaseCardComponent';
import {Router} from '@angular/router';
import {UserService} from '../../common/services/user.service';

@Component({
  selector: 'app-card-left-bar',
  templateUrl: './card-left-bar.component.html',
  styleUrls: ['./card-left-bar.component.scss']
})
export class CardLeftBarComponent extends BaseCardComponent implements OnInit {
  @Input() disabledEvent: boolean;
  @Input() userName: string;
  constructor(router: Router,
              renderer: Renderer2,
              userService: UserService) {
    super(renderer, router, userService);
  }

  ngOnInit(): void {
  }


  public goQR(): void {
    this.router.navigate(['/qr', this.userName]);
  }

}
