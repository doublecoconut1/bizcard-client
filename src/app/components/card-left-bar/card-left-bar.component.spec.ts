import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardLeftBarComponent } from './card-left-bar.component';

describe('CardLeftBarComponent', () => {
  let component: CardLeftBarComponent;
  let fixture: ComponentFixture<CardLeftBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardLeftBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardLeftBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
