import { Directive, EventEmitter, Output, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../common/services/user.service';
import { IUserContactInfo } from '../common/interfaces/IUser.interface';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export class BaseCardComponent {
  @Output() public saveToHome$ = new EventEmitter();
  @Output() public share$ = new EventEmitter();

  public userName: string;
  public showBar = false;
  public contactInfo: IUserContactInfo[] = [];
  public selectedObject: any = { index: null };
  public showIcon = true;

  constructor(
    public renderer: Renderer2,
    public router: Router,
    public userService: UserService
  ) {}

  public toggleClass(event: any): void {
    const [childUl] = event.target.parentElement.getElementsByTagName('ul');
    const hasClass = childUl?.classList.contains('active');
    hasClass
      ? this.renderer.removeClass(childUl, 'active')
      : this.renderer.addClass(childUl, 'active');
  }

  public saveToHome($event: MouseEvent): void {
    this.toggleClass($event);
    this.saveToHome$.emit(this.userName);
  }

  public getDimensions(): { width: number; height: number } {
    return window.parent !== window
      ? {
          width: document.documentElement.clientWidth,
          height: document.documentElement.clientHeight,
        }
      : {
          width: window.screen.width,
          height: window.screen.height,
        };
  }

  public getScaleForElementInWindow(
    element: HTMLElement,
    gap: number = 0,
    max: number = 1
  ): number {
    const dimensions = this.getDimensions();
    const elementRect = element.getBoundingClientRect();
    return Math.min(
      (dimensions.width - elementRect.x - gap) / elementRect.width,
      max
    );
  }

  public handleDesignYoursClick(): void {
    const isIframe = window.parent !== window;
    if (isIframe) {
      window.parent.postMessage(
        {
          type: 'action',
          action: 'goToMain',
        },
        '*'
      );
    } else {
      this.router.navigateByUrl('/');
    }
  }

  public handleShareClick(): void {
    this.share$.emit(this.userName);
  }

  public setTitle(name: string): any {
    switch (name) {
      case 'phone':
        return {
          title: 'Phone',
          btnText: 'Call Now',
          linkHref: 'tel:',
          headerText: 'Phone Number:',
        };
      case 'email':
        return {
          title: 'E-mail',
          btnText: 'Send Email',
          linkHref: 'mailto:',
          headerText: 'E-mail',
        };
      case 'website':
        return {
          title: 'Website',
          btnText: 'Go',
          linkHref: '',
          headerText: 'Website',
        };
      case 'address':
        return {
          title: 'Location',
          btnText: 'Locate',
          linkHref: 'http://maps.google.com/?q=',
          headerText: 'Location',
        };
    }
  }

  public modifiedContactInfo(user): void {
    this.contactInfo = [
      {
        key: 'phone',
        value: user.contact.phone,
        ...this.setTitle('phone'),
      },
      {
        key: 'email',
        value: user.contact.email,
        ...this.setTitle('email'),
      },
      {
        key: 'website',
        value: user.contact.website,
        ...this.setTitle('website'),
      },
    ];
    this.contactInfo = this.contactInfo.filter((c) => c.value);
    if (!this.userService.isEmptyObject(user.location)) {
      const sortObject = this.userService.orderObject(user.location);
      const address = Object.values(sortObject)
        .filter((el) => el !== null && el !== '')
        .join(', ');
      address.length
        ? this.contactInfo.push({
            key: 'address',
            value: address.concat(', US'),
            ...this.setTitle('address'),
          })
        : null;
    }
  }

  public showHideBar(): void {
    this.showIcon = !this.showIcon;
    this.showBar = !this.showBar;
    this.showBar && (this.selectedObject.index = null);
  }

  protected scaleToFitParent(element: HTMLElement, scale: number): void {
    element.style.transform = `scale(${scale})`;
  }

  protected scaleFontToFitParent(element: HTMLElement, scale: number): void {
    const fontSize = parseFloat(
      window.getComputedStyle(element, null).getPropertyValue('font-size')
    );
    element.style.fontSize = `${Math.floor(fontSize * scale)}px`;
  }
}
