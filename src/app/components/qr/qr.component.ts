import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.scss']
})
export class QrComponent implements OnInit {
  public url: string;

  @Input() public size = 221;

  constructor(
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    const userName = this.activatedRoute.snapshot.paramMap.get('name');
    this.url = `${window.origin}/#/${userName}`;
  }
}
