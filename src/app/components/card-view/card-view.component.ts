import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { User, UserSocialLinks } from '../../common/interfaces/IUser.interface';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { CardType } from '../../common/enums/card-types.enum';
import { BaseCardComponent } from '../BaseCardComponent';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../common/services/user.service';
import { isMobile } from '../../common/helpers/animations';
import { getStyle } from '../../common/helpers/styles';
import { timer } from 'rxjs';

export enum MenuCategory {
  Share = 1,
  Save,
}

@Component({
  selector: 'app-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.scss'],
})
export class CardViewComponent
  extends BaseCardComponent
  implements OnInit, AfterViewInit {
  public showMenu = false;
  @Input() public image: SafeUrl;
  @Input() public card: CardType;
  @Input() public user: Partial<User>;
  @Input() public lastName: string;
  @Input() public firstName: string;
  @Input() public showSocials: boolean;
  @Input() public parentFrameUserName: string;
  @Input() public disabledEvent: boolean;
  public userName: string;
  public isShow: boolean;
  public userLinks: UserSocialLinks;
  public socialNoScroll: boolean;
  public cardType = CardType;
  public selectedMenuCategory: MenuCategory;
  public MenuCategory = MenuCategory;
  public loadingWithDelay: boolean;
  @ViewChild('contactEl', { read: ElementRef, static: true })
  private contactEl: ElementRef;
  @ViewChild('socialBlock', { read: ElementRef, static: false })
  private socialBlock: ElementRef;
  @ViewChild('avatar', { read: ElementRef, static: false })
  private avatarElement: ElementRef;

  constructor(
    router: Router,
    renderer: Renderer2,
    userService: UserService,
    private cd: ChangeDetectorRef,
    private domSanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute
  ) {
    super(renderer, router, userService);
  }

  @HostListener('document:click', ['$event'])
  onClick(e: MouseEvent): void {
    const event = e as any;
    const clickedInside = (event.path || event.composedPath()).includes(
      this.contactEl.nativeElement
    );
    if (!clickedInside && this.contactInfo.length > 1) {
      this.selectedObject.index = null;
      this.checkForNarrowScreenWithLongHeight();
    }
  }

  public setSelectedMenuCategory(category: MenuCategory): void {
    if (this.selectedMenuCategory === category) {
      this.selectedMenuCategory = null;
    } else {
      this.selectedMenuCategory = category;
    }
  }

  ngOnInit(): void {
    this.isShow = isMobile();
    this.initUserLinks();
    this.initUserName();
    this.modifiedContactInfo(this.user);
    // this.card = CardType.Modern;
    // this.showSocials = false;
    /* for local testing of modern design
     this.card = CardType.Modern;
     this.userLinks = {};
     this.showSocials = false;
      this.contactInfo.length = 3;
     */
    if (this.contactInfo.length === 1) {
      this.handleContactClick(0);
    }
  }

  ngAfterViewInit(): void {
    const block = this.socialBlock?.nativeElement;
    this.socialNoScroll = block?.scrollWidth <= block?.clientWidth;
    this.avatarResize();
    this.resizeName();
    this.cd.detectChanges();
  }

  public toggleMenu(): void {
    this.showMenu = !this.showMenu;
    this.selectedMenuCategory = null;
  }

  public handleContactClick(i: number): void {
    if (this.showMenu) {
      this.showMenu = false;
      this.loadingWithDelay = true;
      timer(400).subscribe(() => {
        this.resizeName(this.card === CardType.Modern);
        this.loadingWithDelay = false;
        this.cd.detectChanges();
        this.checkForNarrowScreenWithLongHeight();
      });
    } else {
      this.resizeName(this.card === CardType.Modern);
      this.cd.detectChanges();
      this.selectedObject.index = i;
      this.checkForNarrowScreenWithLongHeight();
    }
    this.selectedObject.index = i;
    this.avatarElement.nativeElement.style.width = null;
    this.avatarElement.nativeElement.style.height = null;
  }

  public goQR(): void {
    this.router.navigate(['/qr', this.userName]);
  }

  public getSmsStringFromNumber(numberString: string): SafeUrl {
    return this.domSanitizer.bypassSecurityTrustUrl(
      `sms://${numberString.replace(/([^0-9])/g, '')}`
    );
  }

  public getSmsStringForCurrentUrl(): SafeUrl {
    return this.domSanitizer.bypassSecurityTrustUrl(
      `sms: ?&body=${window.origin}/#/${
        this.parentFrameUserName || this.userName
      }`
    );
  }

  private checkForNarrowScreenWithLongHeight(): void {
    if (
      document.documentElement.clientHeight >= 630 &&
      document.documentElement.clientWidth <= 414 &&
      this.card === CardType.Modern
    ) {
      if (this.selectedObject.index == null) {
        this.contactEl.nativeElement.style = ``;
      } else {
        this.contactEl.nativeElement.style = ``;
        this.cd.detectChanges();
        const contacts = this.contactEl.nativeElement.getBoundingClientRect();
        const scale =
          1 +
          document.documentElement.clientWidth / contacts.width -
          (this.showSocials ? 1.05 : 1.14);
        this.contactEl.nativeElement.style = `transform: scale(${scale}); transform-origin: bottom`;
      }
    }
  }

  private initUserLinks(): void {
    this.userLinks = this.user.socialLinks as UserSocialLinks;
  }

  private initUserName(): void {
    this.userName = this.activatedRoute.snapshot.paramMap.get('name');
  }

  private avatarResize(): void {
    if (this.card === CardType.Modern) {
      const {
        height: parentElementHeight,
      } = this.avatarElement.nativeElement.parentElement.getBoundingClientRect();

      const {
        height: nextElementHeight,
      } = this.avatarElement.nativeElement.nextElementSibling.getBoundingClientRect();

      const elementSize =
        Math.min(
          document.documentElement.clientWidth - 80,
          parseInt(String(parentElementHeight - nextElementHeight), 10)
        ) - 71;

      document.documentElement.style.setProperty(
        '--cardBgBlockContentWidthBefore',
        `${elementSize}px`
      );
      document.documentElement.style.setProperty(
        '--cardBgBlockContentHeightJs',
        `${elementSize}px`
      );
      document.documentElement.style.setProperty(
        '--cardBgBlockContentWidthJs',
        `${elementSize}px`
      );
    }
  }

  private resizeName(resetStyles = false): void {
    Array.from(document.querySelectorAll('.card_info_name'))
      .filter((c: any) => c.offsetWidth < c.scrollWidth)
      .forEach((c: any) => {
        const scale = Math.max(
          0.1,
          Math.min(1 - (c.scrollWidth - c.offsetWidth) / c.scrollWidth, 1)
        );
        c.childNodes.forEach((cc) => {
          const fontSize = parseInt(getStyle(cc, 'font-size'), 10);
          cc.style =
            scale < 1 && !resetStyles
              ? `font-size:${Math.floor(fontSize * scale)}px`
              : '';
        });
      });
  }
}
