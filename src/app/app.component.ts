import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router, RouterOutlet} from '@angular/router';
import {isMobile, routeAnimation} from './common/helpers';
import {UserService} from './common/services/user.service';
import {IframeManagerService} from './common/services/iframe-manager.service';
import {filter} from 'rxjs/operators';

declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeAnimation]
})
export class AppComponent implements OnInit {
  title = 'Fizzcard';
  isMobile: boolean;

  constructor(
    userService: UserService,
    iframeManagerService: IframeManagerService,
    private router: Router
  ) {
    iframeManagerService.initialize();
    userService.initialize();
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      gtag('config', 'UA-182689342', {
        page_path: event.urlAfterRedirects
      });
    });
  }

  ngOnInit(): void {
    this.isMobile = isMobile();
  }

  prepareRoute(outlet: RouterOutlet): RouterOutlet {
    return outlet.activatedRouteData.state ? outlet.activatedRouteData.state : '';
  }

}
